using UnityEngine;

namespace Game {

    public class InputManager : MonoBehaviour {

        [SerializeField]
        private Pawn _pawn;

        [SerializeField]
        private Target _defaultTarget;

        [SerializeField]
        private Camera _camera;

        [SerializeField]
        private LayerMask _targetsMask;

        [SerializeField]
        private float _raycastDistance = 10f;


        private Input _input;

        private void Awake() {
            _input = new Input();
            _input.Player.Click.performed += OnPlayerClick;
            _input.Player.ReturnToPosition.performed += OnPlayerCallToReturn;
            _input.Enable();
        }

        private void OnPlayerClick(UnityEngine.InputSystem.InputAction.CallbackContext context) {
            var pointerPosition = _input.Player.PointerPosition.ReadValue<Vector2>();
            var screenToViewportPoint = _camera.ScreenToViewportPoint(pointerPosition);
            if (screenToViewportPoint.x >= 0f && screenToViewportPoint.x <= 1f &&
                screenToViewportPoint.y >= 0f && screenToViewportPoint.y <= 1f) {

                var ray = _camera.ScreenPointToRay(pointerPosition);
                if (Physics.Raycast(ray, out var hit, _raycastDistance, _targetsMask)) {
                    if (hit.collider.TryGetComponent<Target>(out var target)) {
                        _pawn.MoveToTarget(target);
                    }
                }
            }
        }

        private void OnPlayerCallToReturn(UnityEngine.InputSystem.InputAction.CallbackContext context) {
            _pawn.MoveToTarget(_defaultTarget);
        }
    }
}