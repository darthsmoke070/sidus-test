using System.Collections;
using UnityEngine;
using Tools;

namespace Game {

    public class Pawn : MonoBehaviour {

        [SerializeField]
        private AnimationCurve _movementCurve;

        [SerializeField]
        private float _movementTime;

        private Target _target;

        public void MoveToTarget(Target target) {
            StopAllCoroutines();
            StartCoroutine(MoveToTargetCoroutine(target));
        }

        private IEnumerator MoveToTargetCoroutine(Target target) {
            if (_target != null) {
                _target.SetActive(true);
            }
            _target = target;
            
            var timer = 0f;
            var startedPos = transform.position;
            var startedRot = transform.rotation;
            while (timer <= _movementTime) {
                var dt = Time.deltaTime;
                var curvedTime = _movementCurve.Evaluate(timer / _movementTime);
                transform.position = MathTool.Lerp(startedPos, target.transform.position, curvedTime);
                transform.rotation = MathTool.Lerp(startedRot, target.transform.rotation, curvedTime);
                timer += dt;
                yield return new WaitForEndOfFrame();
            }
            
            _target.SetActive(false);
        }
    }
}
