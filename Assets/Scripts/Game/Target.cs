using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Game {

    public class Target : MonoBehaviour {

        [SerializeField]
        private GameObject _visual;

        [SerializeField]
        private Collider _collider;

        public void SetActive(bool enabled) {
            if (_visual != null) {
                _visual.SetActive(enabled);
            }
            if (_collider != null) {
                _collider.enabled = enabled;
            }
        }
    }
}
