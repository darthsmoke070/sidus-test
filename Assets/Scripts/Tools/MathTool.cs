using UnityEngine;

namespace Tools {

    public static class MathTool {

        public static Vector3 Lerp(Vector3 from, Vector3 to, float t) {
            return new Vector3(Lerp(from.x, to.x, t), Lerp(from.y, to.y, t), Lerp(from.z, to.z, t));
        }

        public static float Lerp(float from, float to, float t) {
            return from + (to - from) * t;
        }

        // https://www.euclideanspace.com/maths/algebra/realNormedAlgebra/quaternions/slerp/index.htm
        public static Quaternion Lerp(Quaternion from, Quaternion to, float t) {
            var cosHalfTheta = from.w * to.w + from.x * to.x + from.y * to.y + from.z * to.z;
            if (Mathf.Abs(cosHalfTheta) > 1.0f) {
                return from;
            }

            var halfTheta = Mathf.Acos(cosHalfTheta);
            var sinHalfTheta = Mathf.Sqrt(1.0f - cosHalfTheta * cosHalfTheta);
            if (Mathf.Abs(sinHalfTheta) < 0.001f) {
                return new Quaternion(from.x * 0.5f + to.x * 0.5f,
                                      from.y * 0.5f + to.y * 0.5f,
                                      from.z * 0.5f + to.z * 0.5f,
                                      from.w * 0.5f + to.w * 0.5f);
            }

            var ratioA = Mathf.Sin((1f - t) * halfTheta) / sinHalfTheta;
            var ratioB = Mathf.Sin(t * halfTheta) / sinHalfTheta;
            return new Quaternion(from.x * ratioA + to.x * ratioB,
                                  from.y * ratioA + to.y * ratioB,
                                  from.z * ratioA + to.z * ratioB,
                                  from.w * ratioA + to.w * ratioB);
        }
    }
}